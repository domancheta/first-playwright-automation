import { test, expect } from '@playwright/test';

test('basic test', async ({ page }) => {
  await page.goto('https://prowand.pro-unlimited.com/login.html');
  await page.fill('#usernamefield', 'domancheta@gmail.com');
  await page.fill('#passwordfield', 'D0rito$AreT@sty');
  const button = page.locator('#loginButton');
  await button.click();

  await page.selectOption('select#selectedBillingType', 'Time');
  // select first date range in dropdown
  await page.selectOption('select#dateRangeString', {index: 1})
  const submit_button =  page.locator('[src="/media/images/but_submit.gif"]')
  await submit_button.click()
  
  for (let i = 1; i <= 5; i++) {
  } 
  // Monday
  await page.selectOption('select[name="billingDetailItems[1].billingTimeSpans[0].startHourM"]', '9')  
  await page.selectOption('select[name="billingDetailItems[1].billingTimeSpans[0].endHourM"]', '11')  
  await page.locator('#billingDtls1 >> text=Add New').click()

  await page.selectOption('select[name="billingDetailItems[1].billingTimeSpans[1].startHourM"]', '11')
  await page.selectOption('select[name="billingDetailItems[1].billingTimeSpans[1].endHourM"]', '12')  
  await page.selectOption('select[name="billingDetailItems[1].billingTimeSpans[1].endMeridiem"]', {label: 'PM'})
  await page.selectOption('select[name="billingDetailItems[1].billingTimeSpans[1].timeEntrySpanType"]', {label: 'Lunch'})
  await page.locator('#billingDtls1 >> text=Add New').click()

  await page.selectOption('select[name="billingDetailItems[1].billingTimeSpans[2].startHourM"]', '12')
  await page.selectOption('select[name="billingDetailItems[1].billingTimeSpans[2].startMeridiem"]', {label: 'PM'})
  await page.selectOption('select[name="billingDetailItems[1].billingTimeSpans[2].endHourM"]', '6')  
  await page.selectOption('select[name="billingDetailItems[1].billingTimeSpans[2].endMeridiem"]', {label: 'PM'})
  await page.locator('select[name="billingDetailItems[1].billingTimeSpans[2].endMeridiem"]').click()
  

  // Tuesday
  await page.selectOption('select[name="billingDetailItems[2].billingTimeSpans[0].startHourM"]', '9')  
  await page.selectOption('select[name="billingDetailItems[2].billingTimeSpans[0].endHourM"]', '11')  
  await page.locator('#billingDtls2 >> text=Add New').click()

  await page.selectOption('select[name="billingDetailItems[2].billingTimeSpans[1].startHourM"]', '11')
  await page.selectOption('select[name="billingDetailItems[2].billingTimeSpans[1].endHourM"]', '12')  
  await page.selectOption('select[name="billingDetailItems[2].billingTimeSpans[1].endMeridiem"]', {label: 'PM'})
  await page.selectOption('select[name="billingDetailItems[2].billingTimeSpans[1].timeEntrySpanType"]', {label: 'Lunch'})
  await page.locator('#billingDtls2 >> text=Add New').click()

  await page.selectOption('select[name="billingDetailItems[2].billingTimeSpans[2].startHourM"]', '12')
  await page.selectOption('select[name="billingDetailItems[2].billingTimeSpans[2].startMeridiem"]', {label: 'PM'})
  await page.selectOption('select[name="billingDetailItems[2].billingTimeSpans[2].endHourM"]', '6')  
  await page.selectOption('select[name="billingDetailItems[2].billingTimeSpans[2].endMeridiem"]', {label: 'PM'})
  await page.locator('select[name="billingDetailItems[2].billingTimeSpans[2].endMeridiem"]').click();

  // Wednesday
  await page.selectOption('select[name="billingDetailItems[3].billingTimeSpans[0].startHourM"]', '9')  
  await page.selectOption('select[name="billingDetailItems[3].billingTimeSpans[0].endHourM"]', '11')  
  await page.locator('#billingDtls3 >> text=Add New').click()

  await page.selectOption('select[name="billingDetailItems[3].billingTimeSpans[1].startHourM"]', '11')
  await page.selectOption('select[name="billingDetailItems[3].billingTimeSpans[1].endHourM"]', '12')  
  await page.selectOption('select[name="billingDetailItems[3].billingTimeSpans[1].endMeridiem"]', {label: 'PM'})
  await page.selectOption('select[name="billingDetailItems[3].billingTimeSpans[1].timeEntrySpanType"]', {label: 'Lunch'})
  await page.locator('#billingDtls3 >> text=Add New').click()

  await page.selectOption('select[name="billingDetailItems[3].billingTimeSpans[2].startHourM"]', '12')
  await page.selectOption('select[name="billingDetailItems[3].billingTimeSpans[2].startMeridiem"]', {label: 'PM'})
  await page.selectOption('select[name="billingDetailItems[3].billingTimeSpans[2].endHourM"]', '6')  
  await page.selectOption('select[name="billingDetailItems[3].billingTimeSpans[2].endMeridiem"]', {label: 'PM'})
  await page.locator('select[name="billingDetailItems[3].billingTimeSpans[2].endMeridiem"]').click()

  // Thursday
  await page.selectOption('select[name="billingDetailItems[4].billingTimeSpans[0].startHourM"]', '9')  
  await page.selectOption('select[name="billingDetailItems[4].billingTimeSpans[0].endHourM"]', '11')  
  await page.locator('#billingDtls4 >> text=Add New').click()

  await page.selectOption('select[name="billingDetailItems[4].billingTimeSpans[1].startHourM"]', '11')
  await page.selectOption('select[name="billingDetailItems[4].billingTimeSpans[1].endHourM"]', '12')  
  await page.selectOption('select[name="billingDetailItems[4].billingTimeSpans[1].endMeridiem"]', {label: 'PM'})
  await page.selectOption('select[name="billingDetailItems[4].billingTimeSpans[1].timeEntrySpanType"]', {label: 'Lunch'})
  await page.locator('#billingDtls4 >> text=Add New').click()

  await page.selectOption('select[name="billingDetailItems[4].billingTimeSpans[2].startHourM"]', '12')
  await page.selectOption('select[name="billingDetailItems[4].billingTimeSpans[2].startMeridiem"]', {label: 'PM'})
  await page.selectOption('select[name="billingDetailItems[4].billingTimeSpans[2].endHourM"]', '6')  
  await page.selectOption('select[name="billingDetailItems[4].billingTimeSpans[2].endMeridiem"]', {label: 'PM'})
  await page.locator('select[name="billingDetailItems[4].billingTimeSpans[2].endMeridiem"]').click()

  // Friday
  await page.selectOption('select[name="billingDetailItems[5].billingTimeSpans[0].startHourM"]', '9')  
  await page.selectOption('select[name="billingDetailItems[5].billingTimeSpans[0].endHourM"]', '11')  
  await page.locator('#billingDtls5 >> text=Add New').click()

  await page.selectOption('select[name="billingDetailItems[5].billingTimeSpans[1].startHourM"]', '11')
  await page.selectOption('select[name="billingDetailItems[5].billingTimeSpans[1].endHourM"]', '12')  
  await page.selectOption('select[name="billingDetailItems[5].billingTimeSpans[1].endMeridiem"]', {label: 'PM'})
  await page.selectOption('select[name="billingDetailItems[5].billingTimeSpans[1].timeEntrySpanType"]', {label: 'Lunch'})
  await page.locator('#billingDtls5 >> text=Add New').click()

  await page.selectOption('select[name="billingDetailItems[5].billingTimeSpans[2].startHourM"]', '12')
  await page.selectOption('select[name="billingDetailItems[5].billingTimeSpans[2].startMeridiem"]', {label: 'PM'})
  await page.selectOption('select[name="billingDetailItems[5].billingTimeSpans[2].endHourM"]', '6')  
  await page.selectOption('select[name="billingDetailItems[5].billingTimeSpans[2].endMeridiem"]', {label: 'PM'})
  await page.locator('select[name="billingDetailItems[5].billingTimeSpans[2].endMeridiem"]').click()

  await page.pause()


});